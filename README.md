# trabalho insertion sort

Esse codigo é parte de uma apresentação realizada na disciplina de Algoritmo e estruturas de dados II

## Clonagem

### Via ssh

```bash
git clone git@bitbucket.org:jorgeluislopes/trabalho-insertion-sort.git
```

### Via https

```bash
git clone https://jorgeluislopes@bitbucket.org/jorgeluislopes/trabalho-insertion-sort.git
```

## Execução

### Execução do main.c

```bash
make run-main
```

### Execução do complexidade.c

```bash
make run-complexidade
```

