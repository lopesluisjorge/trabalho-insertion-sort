#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "insertionsort.h"

#define MAX 10000

void vetor_aleatorio(int *aleatorio, int tam);

int main(int argc, char const *argv[])
{
	int ordenado[MAX], reverso[MAX], aleatorio[MAX];
	int i;

	// MELHOR CASO
	for (i = 0; i < MAX; i++) ordenado[i] = i;
	clock_t inicio_ordenado = clock();
	insertion_sort(ordenado, MAX);
	clock_t fim_ordenado = clock();

	// PIOR CASO
	for (i = 0; i < MAX; i++) reverso[i] = MAX - i;
	clock_t inicio_reverso = clock();
	insertion_sort(reverso, MAX);
	clock_t fim_reverso = clock();

	// CASO NORMAL
	vetor_aleatorio(aleatorio, MAX);
	clock_t inicio_aleatorio = clock();
	insertion_sort(aleatorio, MAX);
	clock_t fim_aleatorio = clock();

	printf("\nOrdenacao do vetor ordenado executou em %f segundos\n", ((double) fim_ordenado - inicio_ordenado) / CLOCKS_PER_SEC);
	printf("\nNumero de computações necessarias para ordenar vetor ordenado: %d\n", fim_ordenado - inicio_ordenado);

	printf("\nOrdenacao do vetor reverso executou em %f segundos\n", ((double) fim_reverso - inicio_reverso) / CLOCKS_PER_SEC);
	printf("\nNumero de computações necessarias para ordenar vetor reverso: %d\n", fim_reverso - inicio_reverso);

	printf("\nOrdenacao do vetor aleatorio executou em %f segundos\n", ((double) fim_aleatorio - inicio_aleatorio) / CLOCKS_PER_SEC);
	printf("\nNumero de computações necessarias para ordenar vetor aleatorio: %d\n", fim_aleatorio - inicio_aleatorio);

	return 0;
}

void vetor_aleatorio(int *aleatorio, int tam) {
	int i;

	srand(time(NULL)); 
	
	for (i = 0; i < tam; i++) aleatorio[i] = rand() % 99 + 1;
}

