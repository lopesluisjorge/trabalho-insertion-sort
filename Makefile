program-teste-mesa: codigo.o
	gcc -o program-teste-mesa codigo.o -lm

program-complexidade: complexidade.o insertionsort.o
	gcc -o program-complexidade complexidade.o insertionsort.o -lm

program-main: main.o insertionsort.o
	gcc -o program-main main.o insertionsort.o -lm

codigo.o: codigo.c
	gcc -c codigo.c

complexidade.o: complexidade.c
	gcc -c complexidade.c

main.o: main.c
	gcc -c main.c

insertionsort.o: insertionsort.c
	gcc -c insertionsort.c

run-teste-mesa: program-teste-mesa
	./program-teste-mesa

run-complexidade: program-complexidade
	./program-complexidade

run-main: program-main
	./program-main

clean:
	rm -rf *.out *.o program-*
