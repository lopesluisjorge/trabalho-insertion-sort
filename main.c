#include <stdio.h>
#include <stdlib.h>

#include "insertionsort.h"

int main(int argc, char const *argv[])
{
	int v[] = {25, 32, 64, 15, 8, 2, 4, 9, 3, 6};
	int i;

	insertion_sort(v, 10);

	for (i = 0; i < 10; i++) {
		printf("%d\n", v[i]);
	}

	return 0;
}
