#include "insertionsort.h"

void insertion_sort(int *vector, int tam) {
	int i, j, aux;

	for (i = 1; i < tam; i++) {
		aux = vector[i];
		j = i - 1;

		while ((j >= 0) && (aux < vector[j])) {
			vector[j+1] = vector[j];
			j--;
		}

		vector[j+1] = aux;
	}	
}
