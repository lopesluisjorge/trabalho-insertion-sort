#include <stdio.h>
#include <stdlib.h>


int * insertion_sort(int *vector, int tam);

int main(int argc, char const *argv[])
{
	int v[] = {4, 2, 5, 8, 1};
	int i;

	int *ord = insertion_sort(v, 5);

	printf("\n");

	printf("[ORDENADO]\n");

	for (i = 0; i < 5; i++) {
		printf("%d.. ", ord[i]);
	}

	printf("\n");

	return 0;
}

int * insertion_sort(int *vector, int tam) {
	int i, j, k, aux;

	int* novo = (int *) malloc(sizeof(int) * tam);
	
	printf("[DESORDENADO]\n");

	for (k = 0; k < tam; k++) printf("%d.. ", vector[k]);

	printf("[INICIO]\n");
	
	novo[0] = vector[0];

	printf("%d.. \n", vector[0]);
	
	for (i = 1; i < tam; i++) {
		aux = vector[i];
		j = i - 1;

		while ((j >= 0) && (aux < novo[j])) {
			novo[j+1] = novo[j];
			j--;


			for (k = 0; k < tam; k++) printf("%d.. ", novo[k]);

			printf(" [Ordenando %d]\n", vector[i]);
		}
		
		novo[j+1] = aux;

		for (k = 0; k < tam; k++) printf("%d.. ", novo[k]);

		printf("\n\n");
	}

	return novo;
}
